import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LocationsListComponent } from './locations-list.component';
import {Store, StoreModule} from "@ngrx/store";
import {AppState, locationsReducer} from "../store/reducers";
import {AddLocationAction, DeletedLocationAction, LocationsActions} from "../store/actions";

describe('LocationsListComponent', () => {
  let component: LocationsListComponent;
  let fixture: ComponentFixture<LocationsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LocationsListComponent ],
      imports: [ StoreModule.forRoot({
        locations: locationsReducer
      })],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LocationsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
  describe('removeLocationAction', () => {
    it('should dispatch remove location action', () => {
      const expectedAction = new DeletedLocationAction();
      const store = jasmine.createSpyObj<Store<AppState>>('store', ['dispatch']);

      const locationsActions = new DeletedLocationAction();
      store.dispatch(locationsActions);
      expect(store.dispatch).toHaveBeenCalledWith(expectedAction);
    });
  });
  describe('addLocationAction', () => {
    it('should dispatch add location action', () => {
      const expectedAction = new AddLocationAction();
      const store = jasmine.createSpyObj<Store<AppState>>('store', ['dispatch']);

      const locationsActions = new AddLocationAction();
      store.dispatch(locationsActions);
      expect(store.dispatch).toHaveBeenCalledWith(expectedAction);
    });
  });
});
