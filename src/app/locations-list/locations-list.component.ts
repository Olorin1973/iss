import {Component, OnDestroy, OnInit} from "@angular/core";
import {Store, select} from "@ngrx/store";
import {Observable, Subscription} from "rxjs";
import {ISavedLocation} from "../app.component";
import {AppState} from "../store/reducers";
import {filter} from "rxjs/operators";
import {
  AddLocationAction,
  DeletedLocationAction,
  FlyToLocationAction,
  RestoreLocationAction, SearchAction,
  StartPolling
} from "../store/actions";
import {getSearchedLocations} from "../store/selectors";

@Component({
  selector: "app-locations-list",
  templateUrl: "./locations-list.component.html",
  styleUrls: ["./locations-list.component.scss"]
})
export class LocationsListComponent implements OnInit, OnDestroy {
  locations$: Observable<any>;
  selectedLocation: ISavedLocation | undefined;
  subscriptions: Subscription[] = [];
  private lastDeletedLocation: ISavedLocation | undefined = undefined;

  constructor(
    private store: Store<AppState>) {
    this.locations$ = this.store.select(getSearchedLocations);
    if (window.localStorage.getItem("locationsList")) {
      const locationsList = JSON.parse(
        <string>window.localStorage.getItem("locationsList")
      );
      locationsList.forEach((location: any) => {
        this.store.dispatch((new AddLocationAction(location)));
      });
    }
    this.store.dispatch((new SearchAction('')));
    this.subscriptions.push(this.locations$.pipe(filter(Boolean))
      .subscribe((result: any) => {
        if (result?.length > 0) {
          if (!this.selectedLocation) {
            this.selectedLocation = result[result.length - 1];
          }
        }
        window.localStorage.setItem("locationsList", JSON.stringify(result));
      }));
    document.addEventListener('keydown', (event) => {
      if (event.ctrlKey && event.key === 'z') {
        this.restoreDeletedLocation();
      }
    });
  }

  ngOnInit() {
  }

  flyTolocation(location: ISavedLocation) {
    if (this.selectedLocation?.name === location?.name) {
      this.store.dispatch((new StartPolling(true)));
      return;
    }
    this.selectedLocation = location;
    this.store.dispatch((new FlyToLocationAction(location)));
  }

  removeLocation(location: ISavedLocation) {
    this.lastDeletedLocation = location;
    this.store.dispatch((new DeletedLocationAction(location)));
    this.store.dispatch((new SearchAction('')));
  }

  restoreDeletedLocation() {
    if (this.lastDeletedLocation) {
      this.store.dispatch((new RestoreLocationAction(this.lastDeletedLocation)));
      this.lastDeletedLocation = undefined;
      this.store.dispatch((new SearchAction('')));
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }
}
