import { Component, OnInit } from '@angular/core';

export const environment = {
  mapbox: {
    accessToken: 'pk.eyJ1Ijoia2xhaW5lcnQiLCJhIjoiY2szMWV3NDd3MDd0dDNkbWo0MjVyZ2FvNCJ9.wnNZCmt8MSlhDsAgTOeRjw'
  }
};
@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
