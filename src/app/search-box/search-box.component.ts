import {Component, OnInit, Output, EventEmitter, Input} from "@angular/core";
import {Observable, of, Subject} from "rxjs";
import {ISavedLocation} from "../app.component";
import {Store} from "@ngrx/store";
import {AppState} from "../store/reducers";
import {LoadAction, SearchAction} from "../store/actions";
import {getSearchedLocations} from "../store/selectors";
import {switchMap} from "rxjs/operators";

@Component({
  selector: "app-search-box",
  templateUrl: "./search-box.component.html",
  styleUrls: ["./search-box.component.scss"]
})
export class SearchBoxComponent implements OnInit {
  query: string = '';
  searchInputChange$ = new Subject<string>();

  constructor(private _store: Store<AppState>) {
    this.searchInputChange$
      .pipe(
        switchMap((text: string) => of(text)))
      .subscribe((text: string) => this._store.dispatch(new SearchAction(text)));
  }

  ngOnInit() {
  }
}
