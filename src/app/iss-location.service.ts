import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class IssLocationService {

  constructor(private http: HttpClient) { }

  getISSLocation(): Observable<any>{
    return this.http.get('http://api.open-notify.org/iss-now.json');
  }
}
