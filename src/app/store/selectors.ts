import {AppState} from "./reducers";
import {createSelector} from "@ngrx/store";

export const locations = (state: AppState) => state.locations;
export const selectLocations = (state: AppState) => state.locations;
export const getSearchedLocations = createSelector(selectLocations,
  (savedLocations) => {
    return savedLocations.searchedLocations;
  });
