import {ISavedLocation} from "../app.component";
import {
  ADD_LOCATION,
  DELETE_LOCATION,
  FLY_TO_LOCATION,
  LocationsActions,
  RESTORE_LOCATION,
  SEARCH,
  START_POLLING
} from "./actions";

export interface AppState {
  locations: any;
  searchedLocations: ISavedLocation[];
  flyToLocation: ISavedLocation;
  startPolling: boolean
}

export const initialState = {
  locations: [],
  searchedLocations: [],
  flyToLocation: {name: '', lon: -1, lat: -1, date: new Date()},
  startPolling: true
};

export const locationsReducer = (state: AppState = initialState, action: LocationsActions): AppState => {
  switch (action.type) {
    case ADD_LOCATION:
    case RESTORE_LOCATION:
      return {
        ...state, locations: [...state.locations, action.payLoad]
      };
    case DELETE_LOCATION:
      return {
        ...state, locations: state.locations.filter((item: ISavedLocation) => item.name !== action.payLoad.name)
      };
    case FLY_TO_LOCATION:
      return {
        ...state, flyToLocation: action.payLoad, startPolling: false
      };
    case START_POLLING:
      return {
        ...state, startPolling: action.payLoad
      };
    case SEARCH:
      const keywordContents = action.payLoad?.length === 0 ? state.locations :
        state.locations.filter((location: ISavedLocation) => location.name.toLowerCase().includes(action.payLoad));
      return {
        ...state,
        locations: state.locations,
        searchedLocations: keywordContents
      };
    default:
      return state;
  }
}
