import {Action} from '@ngrx/store';
import {ISavedLocation} from "../app.component";

export const ADD_LOCATION = 'ADD_LOCATION';
export const DELETE_LOCATION = 'DELETE_LOCATION';
export const RESTORE_LOCATION = 'RESTORE_LOCATION';
export const FLY_TO_LOCATION = 'FLY_TO_LOCATION';
export const START_POLLING = 'START_POLLING';
export const LOAD = 'LOAD';
export const LOAD_SUCCESS = 'LOAD_SUCCESS';
export const SEARCH = 'SEARCH';

export class LoadAction implements Action {
  readonly type = LOAD;
  constructor() { }
}

export class LoadActionSuccess implements Action {
  readonly type = LOAD_SUCCESS;
  constructor(public payLoad: ISavedLocation[]) { }
}

export class SearchAction implements Action {
  readonly type = SEARCH as string;
  constructor(public payLoad?: any) {

  }
}

export class AddLocationAction implements Action {
  readonly type = ADD_LOCATION as string;

  constructor(public payLoad?: any) {
  }

}

export class DeletedLocationAction implements Action {
  readonly type = DELETE_LOCATION as string;

  constructor(public payLoad?: any) {
  }
}

export class RestoreLocationAction implements Action {
  readonly type = RESTORE_LOCATION as string;

  constructor(public payLoad?: any) {
  }
}

export class FlyToLocationAction implements Action {
  readonly type = FLY_TO_LOCATION as string;

  constructor(public payLoad?: any) {
  }
}

export class StartPolling implements Action {
  readonly type = START_POLLING as string;

  constructor(public payLoad?: any) {
  }
}

export type LocationsActions =
  AddLocationAction | DeletedLocationAction | RestoreLocationAction | StartPolling | LoadAction
  | LoadActionSuccess
  | SearchAction;
