import { TestBed } from '@angular/core/testing';

import { IssLocationService } from './iss-location.service';
import {HttpClient, HttpClientModule} from "@angular/common/http";

describe('IssLocationService', () => {
  let service: IssLocationService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [HttpClient]
    });
    service = TestBed.inject(IssLocationService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
