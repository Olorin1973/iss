import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { InformationComponent } from './information/information.component';
import { MapAreaComponent } from './map-area/map-area.component';
import { SearchBoxComponent } from './search-box/search-box.component';
import { LocationsListComponent } from './locations-list/locations-list.component';
import { MainComponent } from './main/main.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { StoreModule, ActionReducer, MetaReducer } from '@ngrx/store';
import { localStorageSync } from 'ngrx-store-localstorage';
import { LocationComponent } from './location/location.component';
import { LocationsPipe } from './locations.pipe';
import {locationsReducer} from "./store/reducers";
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DialogComponent } from './dialog/dialog.component';
import {OverlayModule} from '@angular/cdk/overlay';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import {MatTabsModule} from "@angular/material/tabs";
import {MatTableModule} from "@angular/material/table";
export function localStorageSyncReducer(reducer: ActionReducer<any>): ActionReducer<any> {
  return localStorageSync({ keys: ['locations'], rehydrate: true })(reducer);
}
const metaReducers: Array<MetaReducer<any, any>> = [localStorageSyncReducer];
@NgModule({
  declarations: [
    AppComponent,
    InformationComponent,
    MapAreaComponent,
    SearchBoxComponent,
    LocationsListComponent,
    MainComponent,
    LocationComponent,
    LocationsPipe,
    DialogComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    StoreModule.forRoot({
      locations: locationsReducer
    }),
    BrowserAnimationsModule,
    OverlayModule,
    MatDialogModule,
    MatButtonModule,
    MatInputModule,
    MatTabsModule,
    MatTableModule
  ],
  providers: [HttpClient],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponent]
})
export class AppModule { }
