import {Component, OnInit, ChangeDetectorRef, ViewChild, OnDestroy} from "@angular/core";
import {} from 'googlemaps';
import { select, Store } from "@ngrx/store";
import {ISavedLocation} from "../app.component";
import {AddLocationAction, SearchAction} from "../store/actions";
import {IssLocationService} from "../iss-location.service";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import {DialogComponent} from "../dialog/dialog.component";
import {filter} from "rxjs/operators";
import {Observable, Subscription} from "rxjs";
import {AppState} from "../store/reducers";
import {MatTabChangeEvent} from "@angular/material/tabs";

@Component({
  selector: "app-map-area",
  templateUrl: "./map-area.component.html",
  styleUrls: ["./map-area.component.scss"]
})
export class MapAreaComponent implements OnInit, OnDestroy {
  map: google.maps.Map | undefined;
  subscriptions: Subscription[] = [];
  FORMAT_DATE_TIME = 'YYYY-MM-dd HH:mm:ss';
  lastKnownLocation: ISavedLocation = {lat: -1, lon: -1, date: new Date(), name: ''};
  displayedColumns: string[] = ['name', 'longitude', 'latitude', 'date'];
  // @ts-ignore
  private flyToLocation$: Observable<any> = this.store.select(state => state?.locations?.flyToLocation);
  // @ts-ignore
  private startPolling$: Observable<any> = this.store.select(state => state?.locations?.startPolling);
  locations$: Observable<any>;
  private interval: number = -1;
  public locations: ISavedLocation[] = [];

  ngOnInit() {
    this.initMap();
  }

  constructor(
    private dialog: MatDialog,
    private locationService: IssLocationService,
    private store: Store<AppState>
  ) {
    this.locations$ = this.store.select(state => state.locations);
    this.subscriptions.push(this.flyToLocation$.pipe(filter(Boolean))
      .subscribe((result: any) => {
        clearInterval(this.interval);
        this.flyToLocation(result);
      }),
      this.startPolling$.pipe(filter(Boolean))
        .subscribe((result: any) => {
          this.startPolling();
        }),
      this.locations$.pipe(filter(Boolean))
        .subscribe((result: any) => {
        this.locations = result?.locations;
        }));
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach((sub) => sub.unsubscribe());
  }

  saveLocation(name: string) {
    this.lastKnownLocation = {...this.lastKnownLocation, name: name}
    this.store.dispatch((new AddLocationAction(this.lastKnownLocation)));
    this.store.dispatch((new SearchAction('')));
  }

  flyToLocation(location: ISavedLocation) {
    window.clearInterval(this.interval);
    this.lastKnownLocation = location;
    this.map?.setCenter({lat: Number(this.lastKnownLocation?.lat), lng: Number(this.lastKnownLocation?.lon)});
    this.map?.setZoom(6);
  }

  openDialog(): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.disableClose = true;
    dialogConfig.autoFocus = true;

    const dialogRef = this.dialog.open(DialogComponent, dialogConfig);

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.saveLocation(result);
      }
    });
  }

  private initMap() {
    const mapProperties = {
      center: this.lastKnownLocation? {lat: Number(this.lastKnownLocation.lat), lng: Number(this.lastKnownLocation.lon)}: {lat: -34.397, lng: 150.644},
      zoom: 2,
    };
    const elem = document.getElementById("map") as HTMLElement;
    if (elem) {
      this.map = new google.maps.Map(elem, mapProperties);
    }
  }

  tabChanged($event: MatTabChangeEvent) {
    console.log();
    switch ($event?.tab?.textLabel) {
      case 'Report':
        break;
      case 'Map':
        this.initMap();
    }
  }

  private startPolling() {
    this.map?.setZoom(2);
    this.interval = setInterval(() => {
      this.locationService.getISSLocation().toPromise().then((result) => {
        this.lastKnownLocation = {
          name: '',
          lon: result.iss_position?.longitude,
          lat: result?.iss_position?.latitude,
          date: new Date(result.timestamp * 1000)
        };
        if (!this.map) {
          this.initMap();
        }
        this.map?.setCenter({lat: Number(this.lastKnownLocation.lat), lng: Number(this.lastKnownLocation.lon)});
      });
    }, 2000);
  }
}

