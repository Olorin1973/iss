import {Component, Input, OnInit} from '@angular/core';
import {ISavedLocation} from "../app.component";

@Component({
  selector: 'app-location',
  templateUrl: './location.component.html',
  styleUrls: ['./location.component.scss']
})
export class LocationComponent implements OnInit {

  // @ts-ignore
  @Input() location: ISavedLocation;
  FORMAT_DATE_TIME = 'YYYY-MM-dd HH:mm:ss';
  constructor() {
    // this.location = {lon: '32.4.3.22', lat: '12.66.3.11', name: 'My Location', date: new Date()}
  }

  ngOnInit(): void {
  }

}
