import { Component } from '@angular/core';
// import { Feature } from 'geojson';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'gil-map';
}

export interface ISavedLocation{
  name: string;
  lon: number;
  lat: number;
  date: Date
}
